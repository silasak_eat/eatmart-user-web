import styles from '../styles/Home.module.css'
import Image from 'next/image'

export default function Promotion() {
  return <div>
    <div className={styles.header}>
      <Image
        src="/images/logo.png"
        alt="Promotion"
        width={77}
        height={69}
        // layout="responsive"
      />
      <div className={styles.bannerImage}>
        <img
          src="/images/banner.png"
          alt="Promotion"
        />
      </div>

    </div>
    <div className={styles.body}>
      <h3 className="font-bold text-lg font-prompt">
        พรีเซ็นเตอร์ ฟิวเจอร์วิลเลจปักขคณนาออทิสติกแอปพริคอท
      </h3>
      <div className="p-4">
        <ul className="list-disc font-prompt text-gray-500">
          <li>บลูเบอร์รี คอนเทนเนอร์ ไคลแม็กซ์โทรโข่งห่วยโปสเตอร์</li>
          <li>โอเลี้ยงคอลัมนิสต์ ซีอีโอวัจนะเนิร์สเซอรีเซฟตี้สังโฆ ป๋าบลูเบอร์รี่</li>
          <li>โซลาร์แจม มิลค์ง่าว มาม่าราชบัณฑิตยสถาน</li>
          <li>เฮอร์ริเคนดีลเลอร์คอนเทนเนอร์วิวเยอบีรา</li>
        </ul>
        <div className='text-center my-8'>
          <img className="inline-block w-28 h-28 border-4 rounded-2xl border-blue-500" src="https://via.placeholder.com/115x115"/>
        </div>
        <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-4 px-4 rounded w-full">
          <img className="inline-block" src="/images/download.svg" /> <span>Download QR</span>
        </button>
      </div>
    </div>
  </div>
}