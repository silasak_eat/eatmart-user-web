import styles from '../styles/Home.module.css'
import Image from "next/image";
// import liff from "@line/liff";

export default function Login() {
  // const [pictureUrl, setPictureUrl] = useState(logo);
  // const [idToken, setIdToken] = useState("");
  // const [displayName, setDisplayName] = useState("");
  // const [statusMessage, setStatusMessage] = useState("");
  // const [userId, setUserId] = useState("");
  //
  // const logout = () => {
  //   liff.logout();
  //   window.location.reload();
  // }
  //
  // const initLine = () => {
  //   liff.init({ liffId: '1655665373-YAopzeO6' }, () => {
  //     if (liff.isLoggedIn()) {
  //       runApp();
  //     } else {
  //       liff.login();
  //     }
  //   }, err => console.error(err));
  // }
  //
  // const runApp = () => {
  //   const idToken = liff.getIDToken();
  //   setIdToken(idToken);
  //   liff.getProfile().then(profile => {
  //     console.log(profile);
  //     setDisplayName(profile.displayName);
  //     setPictureUrl(profile.pictureUrl);
  //     setStatusMessage(profile.statusMessage);
  //     setUserId(profile.userId);
  //   }).catch(err => console.error(err));
  // }
  //
  // useEffect(() => {
  //   initLine();
  // }, []);

  return <div className={styles.loginBackground}>
    <Image
      src="/images/logo.png"
      alt="Promotion"
      width={77}
      height={69}
      // layout="responsive"
    />
    <p className="mt-4 text-2xl font-bold text-white font-prompt">สมัคร สมาชิกกับเรา</p>
    <p className="text-4xl font-bold text-yellow-300 font-prompt mt-4">รับส่วนลดพิเศษทันที</p>
    <img
      className="mt-4"
      src="/images/banner.png"
      alt="Promotion"
    />
    <div className="w-80 h-14 mt-8 inline-block">
      <div className="flex space-x-4 items-center justify-center flex-1 h-full  py-3 bg-white rounded-md">
        <img src="/images/line.png" />
        <span className="text-base font-semibold text-center text-gray-600 font-prompt">เข้าสู่ระบบด้วย Line</span>
      </div>
    </div>
  </div>
}